#include <iostream>
#include <fstream>
#include <queue>

using namespace std;

int main() {
	ifstream fin("../input.txt");
	queue<int> measurements;
	int current_depth, last_sum = 0, increase_counter = 0;
	for (int i=0; i<3; i++) {
		fin >> current_depth;
		last_sum += current_depth;
		measurements.push(current_depth);
	}
	while (!fin.eof()) {
		measurements.pop();
		fin >> current_depth;
		measurements.push(current_depth);
		queue<int> sum_queue = measurements;
		int sum = 0;
		while (!sum_queue.empty()) {
			sum += sum_queue.front();
			sum_queue.pop();
		}
		if (sum > last_sum) {
			increase_counter++;
		}
		last_sum = sum;
	}
	cout << increase_counter << endl;
	return 0;
}
