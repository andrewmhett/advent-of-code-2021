#include <iostream>
#include <fstream>

using namespace std;

int main() {
	int last_depth, current_depth, increase_counter = 0;
	ifstream fin("../input.txt");
	fin >> last_depth;
	while (!fin.eof()) {
		fin >> current_depth;
		if (current_depth>last_depth) {
			increase_counter++;
		}
		last_depth = current_depth;
	}
	cout << increase_counter << endl;
	return 0;
}
