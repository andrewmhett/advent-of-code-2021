#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <climits>

using namespace std;

int main() {
	vector<int> positions;
	string current_line;
	ifstream fin("../input.txt");
	getline(fin, current_line);
	istringstream ss(current_line);
	string token;
	int max_position = 0;
	int min_position = INT_MAX;
	while (getline(ss, token, ',')) {
		positions.push_back(stoi(token));
		if (stoi(token) > max_position) {
			max_position = stoi(token);
		}
		if (stoi(token) < min_position) {
			min_position = stoi(token);
		}
	}
	int fuel_consumed, min_fuel_consumed = INT_MAX;
	for (int position_1=min_position; position_1<max_position; position_1++) {
		fuel_consumed = 0;
		for (int position_2 : positions) {
			for (int i=1; i<=abs(position_2 - position_1); i++) {
				fuel_consumed += i;
			}
		}
		if (fuel_consumed < min_fuel_consumed) {
			min_fuel_consumed = fuel_consumed;
		}
	}
	cout << min_fuel_consumed << endl;
}
