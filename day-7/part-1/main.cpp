#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <climits>

using namespace std;

int main() {
	vector<int> positions;
	string current_line;
	ifstream fin("../input.txt");
	getline(fin, current_line);
	istringstream ss(current_line);
	string token;
	while (getline(ss, token, ',')) {
		positions.push_back(stoi(token));
	}
	int fuel_consumed, min_fuel_consumed = INT_MAX;
	for (int position_1 : positions) {
		fuel_consumed = 0;
		for (int position_2 : positions) {
			fuel_consumed += abs(position_2 - position_1);
		}
		if (fuel_consumed < min_fuel_consumed) {
			min_fuel_consumed = fuel_consumed;
		}
	}
	cout << min_fuel_consumed << endl;
}
