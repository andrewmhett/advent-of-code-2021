#include <iostream>
#include <fstream>
#include <sstream>
#include <map>

using namespace std;

int main() {
	map<pair<int,int>,int> intersection_map;
	ifstream fin("../input.txt");
	string current_point, token;
	int x1, x2, y1, y2;
	while (!fin.eof()) {
		fin >> current_point;
		if (fin.eof()) break;
		istringstream ss(current_point);
		getline(ss, token, ',');
		x1 = stoi(token);
		getline(ss, token, ',');
		y1 = stoi(token);
		fin >> current_point;
		fin >> current_point;
		ss = istringstream(current_point);
		getline(ss, token, ',');
		x2 = stoi(token);
		getline(ss, token, ',');
		y2 = stoi(token);
		if (!(x1 == x2 || y1 == y2)) continue;
		if (x1 > x2) {
			int swap = x1;
			x1 = x2;
			x2 = swap;
		}
		if (y1 > y2) {
			int swap = y1;
			y1 = y2;
			y2 = swap;
		}
		pair<int,int> coordinates;
		for (int x=x1; x<=x2; x++) {
			for (int y=y1; y<=y2; y++) {
				coordinates.first = x;
				coordinates.second = y;
				intersection_map[coordinates]++;
			}
		}
	}
	int intersection_counter = 0;
	for (auto map_it : intersection_map) {
		if (map_it.second > 1) {
			intersection_counter++;
		}
	}
	cout << intersection_counter << endl;
	return 0;
}
