#include <unordered_set>
#include "board.h"

using namespace std;

Board::Board(int numbers[5][5]) {
	for (int i=0; i<5; i++) {
		for (int o=0; o<5; o++) {
			_numbers[i][o] = numbers[i][o];
		}
	}
}

int  Board::sum_not_called(unordered_set<int> called_numbers) {
	int sum = 0;
	for (int i=0; i<5; i++) {
		for (int o=0; o<5; o++) {
			if (!called_numbers.count(_numbers[i][o])) {
				sum += _numbers[i][o];
			}
		}
	}
	return sum;
}

bool Board::is_game_won(unordered_set<int> called_numbers) {
	bool won = false;
	bool whole_row_called, whole_col_called;
	for (int i=0; i<5; i++) {
		whole_row_called = whole_col_called = true;
		for (int o=0; o<5; o++) {
			if (!called_numbers.count(_numbers[i][o])) {
				whole_row_called = false;
			}
			if (!called_numbers.count(_numbers[o][i])) {
				whole_col_called = false;
			}
		}
		if (whole_row_called || whole_col_called) {
			won = true;
			break;
		}
	}
	return won;
}
