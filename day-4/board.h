#ifndef BOARD_H
#define BOARD_H

#include <unordered_set>

using namespace std;

class Board {
public:
	Board(int[5][5]);
	void call_number(int);
	int sum_not_called(unordered_set<int>);
	bool is_game_won(unordered_set<int>);
private:
	int _numbers[5][5];
};

#endif
