#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <unordered_set>
#include <string.h>
#include "../board.h"

using namespace std;

int main() {
	ifstream fin("../input.txt");
	string current_line;
	int current_number;
	vector<int> drawn_numbers;
	vector<Board> boards;
	getline(fin, current_line);
	istringstream ss(current_line);
	string token;
	while (getline(ss, token, ',')) {
		drawn_numbers.push_back(stoi(token));
	}
	while (!fin.eof()) {
		int board_numbers[5][5];
		getline(fin, current_line);
		for (int i=0; i<5; i++) {
			for (int o=0; o<5; o++) {
				fin >> current_number;
				board_numbers[i][o] = current_number;
			}
		}
		if (fin.eof()) break;
		boards.push_back(Board(board_numbers));
	}
	int final_score = 0;
	unordered_set<int> called_numbers;
	for (int called_num : drawn_numbers) {
		called_numbers.emplace(called_num);
		for (Board board : boards) {
			if (board.is_game_won(called_numbers)) {
				final_score = called_num * board.sum_not_called(called_numbers);
				board.is_game_won(called_numbers);
			}
		}
		if (final_score != 0) {
			break;
		}
	}
	cout << final_score << endl;
	return 0;
}
