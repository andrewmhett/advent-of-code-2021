#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

unsigned long shift_left(unsigned long bins[9]) {
	unsigned long first_bin = bins[0];
	for (unsigned long i=0; i<8; i++) {
		bins[i] = bins[i+1];
	}
	bins[8] = 0;
	return first_bin;
}

unsigned long sum(unsigned long bins[9]) {
	unsigned long sum = 0;
	for (unsigned long i=0; i<9; i++) {
		sum+=bins[i];
	}
	return sum;
}

int main() {
	string current_line;
	unsigned long fish_bins[9] = {0,0,0,0,0,0,0,0,0};
	ifstream fin("../input.txt");
	getline(fin, current_line);
	istringstream ss(current_line);
	string token;
	while (getline(ss, token, ',')) {
		fish_bins[stoi(token)]++;
	}
	for (unsigned long i=0; i<256; i++) {
		unsigned long num_new_fish = shift_left(fish_bins);
		fish_bins[6] += num_new_fish;
		fish_bins[8] = num_new_fish;
	}
	cout << sum(fish_bins) << endl;
	return 0;
}
