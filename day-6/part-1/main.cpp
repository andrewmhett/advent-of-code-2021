#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

int shift_left(int bins[9]) {
	int first_bin = bins[0];
	for (int i=0; i<8; i++) {
		bins[i] = bins[i+1];
	}
	bins[8] = 0;
	return first_bin;
}

int sum(int bins[9]) {
	int sum = 0;
	for (int i=0; i<9; i++) {
		sum+=bins[i];
	}
	return sum;
}

int main() {
	string current_line;
	int fish_bins[9] = {0,0,0,0,0,0,0,0,0};
	ifstream fin("../input.txt");
	getline(fin, current_line);
	istringstream ss(current_line);
	string token;
	while (getline(ss, token, ',')) {
		fish_bins[stoi(token)]++;
	}
	for (int i=0; i<80; i++) {
		int num_new_fish = shift_left(fish_bins);
		fish_bins[6] += num_new_fish;
		fish_bins[8] = num_new_fish;
	}
	cout << sum(fish_bins) << endl;
	return 0;
}
