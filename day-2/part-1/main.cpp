#include <iostream>
#include <fstream>

using namespace std;

int main() {
	ifstream fin("../input.txt");
	string command;
	int value, horizontal_position = 0, depth = 0;
	while (!fin.eof()) {
		fin >> command;
		fin >> value;
		if (fin.eof()) break;
		switch (command[0]) {
		case 'f':
			horizontal_position += value;
			break;
		case 'd':
			depth += value;
			break;
		case 'u':
			depth -= value;
			break;
		}
	}
	cout << horizontal_position * depth << endl;
	return 0;
}
