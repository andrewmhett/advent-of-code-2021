#include <iostream>
#include <fstream>

using namespace std;

int main() {
	ifstream fin("../input.txt");
	string command;
	int value, horizontal_position = 0, depth = 0, aim = 0;
	while (!fin.eof()) {
		fin >> command;
		fin >> value;
		if (fin.eof()) break;
		switch (command[0]) {
		case 'f':
			horizontal_position += value;
			depth += aim * value;
			break;
		case 'd':
			aim += value;
			break;
		case 'u':
			aim -= value;
			break;
		}
	}
	cout << horizontal_position * depth << endl;
	return 0;
}
