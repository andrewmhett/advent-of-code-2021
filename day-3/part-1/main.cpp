#include <iostream>
#include <fstream>
#include <math.h>
#include <cstring>

using namespace std;

int binary_to_decimal(string binary_value) {
	int decimal_value = 0;
	for (int i=11; i>=0; i--) {
		decimal_value += (binary_value[i]-'0') * pow(2,11-i);
	}
	return decimal_value;
}

int main() {
	ifstream fin("../input.txt");
	string current_value;
	int bit_frequencies[12];
	memset(bit_frequencies, 0, sizeof(int) * 12);
	string gamma, epsilon;
	while (!fin.eof()) {
		getline(fin, current_value);
		if (fin.eof()) break;
		for (int i=0; i<12; i++) {
			bit_frequencies[i] += current_value[i]-'0';
		}
	}
	for (int i=0; i<12; i++) {
		if (bit_frequencies[i] > 500) {
			gamma += '1';
			epsilon += '0';
		} else {
			gamma += '0';
			epsilon += '1';
		}
	}
	cout << binary_to_decimal(gamma) * binary_to_decimal(epsilon) << endl;
	return 0;
}
