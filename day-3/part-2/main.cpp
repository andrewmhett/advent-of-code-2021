#include <iostream>
#include <fstream>
#include <math.h>
#include <vector>

using namespace std;

unsigned long binary_to_decimal(string binary_value) {
	unsigned long decimal_value = 0;
	for (int i=11; i>=0; i--) {
		decimal_value += (binary_value[i]-'0') * pow(2,11-i);
	}
	return decimal_value;
}

int main() {
	ifstream fin("../input.txt");
	string current_value;
	vector<string> values;
	string oxygen, co2;
	while (!fin.eof()) {
		getline(fin, current_value);
		if (fin.eof()) break;
		values.push_back(current_value);
	}
	vector<string> oxygen_values = values;
	for (int i=0; i<12; i++) {
		if (oxygen_values.size() == 1) {
			break;
		}
		int one_frequency = 0, zero_frequency = 0;
		for (string value : oxygen_values) {
			if (value[i] == '1') {
				one_frequency++;
			} else {
				zero_frequency++;
			}
		}
		for (auto value_it = oxygen_values.begin(); value_it < oxygen_values.end(); ++value_it) {
			if ((*value_it)[i] == '0' && zero_frequency<=one_frequency) {
				value_it = --oxygen_values.erase(value_it);
			} else if ((*value_it)[i] == '1' && one_frequency<zero_frequency) {
				value_it = --oxygen_values.erase(value_it);
			}
		}
	}
	vector<string> co2_values = values;
	for (int i=0; i<12; i++) {
		if (co2_values.size() == 1) {
			break;
		}
		int one_frequency = 0, zero_frequency = 0;
		for (string value : co2_values) {
			if (value[i] == '1') {
				one_frequency++;
			} else {
				zero_frequency++;
			}
		}
		for (auto value_it = co2_values.begin(); value_it < co2_values.end(); value_it++) {
			if ((*value_it)[i] == '0' && zero_frequency>one_frequency) {
				value_it = --co2_values.erase(value_it);
			} else if ((*value_it)[i] == '1' && one_frequency>=zero_frequency) {
				value_it = --co2_values.erase(value_it);
			}
		}
	}
	cout << binary_to_decimal(oxygen_values[0]) * binary_to_decimal(co2_values[0]) << endl;
	return 0;
}
